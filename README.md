Just for education purpose.


TASK 7 - Starting our Node API app

Moving on from the theme, we’re going to be creating a small NodeJS app which we’ll be connecting with Shopify and using their different APIs. Starting from this task, we’ll start using Git in our workflow.
Start out by creating a new repo in SW Bitbucket. Initialise a node project, set up .gitignore file (you can use https://www.gitignore.io/ if you're not sure which files you need to ignore for nodeJS projects) and do an initial commit and push. After, you should create a new branch, on which you need to install Express to your app and set up a basic router with a route “/products” that returns a simple json object, e.g. { message: “Hello world!” }, when called with GET and a different message when called with POST.  Going to the root URL (“/”), redirects to “/products”. You can test these with curl or Postman.

Bonus points if the returned JSON object changes when your application receives a custom HTTP header (e.g. X-Hello-World) with any value in the request. Since Shopify API authenticates requests using a HTTP header (you can read up on Basic Auth, if you wish, to get a clearer picture), it's good to know what HTTP headers are, and how to use them in your application.

Acceptance Criteria - A link to the repo, with initial node project base and a Pull Request for your new feature branch with others from the team added as reviewers. (Moving on, this is how we’ll continue ) 
